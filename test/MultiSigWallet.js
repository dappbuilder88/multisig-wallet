// Multisig wallet testing assignment
const { ethers } = require('hardhat');
const { expect } = require('chai');
const { parseEther } = require('@ethersproject/units');

describe("Multisig Wallet Assignment testing", () => {

  let contractAbi;

  before(async() => {
    const accounts = await ethers.getSigners();
    const myContract = await ethers.getContractFactory("MultiSigWallet");
    contractAbi = await myContract.deploy([accounts[0].address,accounts[1].address, accounts[2].address], 1);
    await contractAbi.deployed();
    console.log("Contract address: ", contractAbi.address);
  });

  it("Atleast one owner is required", async() => {
    expect(await contractAbi.getOwners()).to.have.length.of.at.least(1);
  });

  it("There must be atleast one required confirmation", async() => {
    expect(await contractAbi.numConfirmationsRequired()).to.greaterThanOrEqual(1);
  });

  it("Owner duplication testing", async () => {
    const findDuplicatesInArray = (arr) => {
      return new Set(arr).size !== arr.length
    }
    const solArr = await contractAbi.getOwners();
    const jsArr = Array.from(solArr);
    const duplicateFound = findDuplicatesInArray(jsArr);
    expect(duplicateFound).to.equal(false);
  });

  it("Owner should not be a zero address", async() => {
    expect(await contractAbi.getOwners()).to.not.include.members(["0x0000000000000000000000000000000000000000"]);
  });

  it("Submit transaction", async () => {
    const accounts = await ethers.getSigners();
    const addressTo = accounts[3].address;
    const value = 300;
    const data = "0xab";
    const wallet = contractAbi.connect(accounts[0]);
    const noOfTransactionsBeforeExecution = await wallet.getTransactionCount();
    await wallet.submitTransaction(addressTo, value, data);
    const noOfTransactionsAfterExecution = await wallet.getTransactionCount();
    expect(noOfTransactionsAfterExecution).is.equal(noOfTransactionsBeforeExecution + 1);
  });
  
  it("Submission test", async () => {
    const accounts = await ethers.getSigners();
    const txIndex = 0;
    const wallet = contractAbi.connect(accounts[0]);
    const transactionsCount = await wallet.getTransactionCount();
    expect(txIndex).lessThan(transactionsCount);
  });

  it("Only owner test", async() => {
    const accounts = await ethers.getSigners();
    const wallet = contractAbi.connect(accounts[2]);
    expect(await wallet.confirmTransaction(0));
  });

  it("Confirmation test", async () => {
    const indexOfTransactionToBeConfirmed = 0;
    const accounts = await ethers.getSigners();
    const msgSender = accounts[2];
    const wallet = await contractAbi.connect(msgSender);
    expect(await wallet.isConfirmed(indexOfTransactionToBeConfirmed,msgSender.address)).to.equal(true);
  });

  it("Deposit test", async () => {
    const value = parseEther("1");
    const options = {value: value}
    const accounts = await ethers.getSigners();
    const msgSender = accounts[4];
    const wallet = await contractAbi.connect(msgSender);
    await wallet.DepositETH(options);
    expect(await contractAbi.getBalance()).to.equal(value);
  });

  
  it("Execution test", async () => {
    const accounts = await ethers.getSigners();
    const msgSender = accounts[2];
    const wallet = await contractAbi.connect(msgSender);
    await wallet.executeTransaction(0);
    const transaction = await wallet.getTransaction(0);
    expect(transaction.executed).to.equal(true);
  });

});